// function bouncer(age){
// 	// if statement
// 	// if statements
// 	if(age < 18){
// 		console.log("You can't come in.")
// 	}else{
// 		console.log("You can come in. ENJOY THE NIGHT!")
// 	}
// }

//Prompt Function

// let age = prompt("Enter your age")

// if (age < 18){
// 	alert("You can't come in.")
// }else{
// 	alert("You can come in. ENJOY THE NIGHT!")
// }


// If statement syntax:
//let num = 1 
// if(condition){

// }
//if(num > 0){
	//code to execute if condition is true
	//if condition is NOT true and there is no else statement, nothing happens.
//}

//If-else statement syntax:

 

//if-else-if statement syntax

// if(condition){
//code here will execute if condition is true
// }else if(condition2){
//code here will execute if condition2 is true(can have as many as you want)
// }else{
	//code here will execute if none of the conditions above are true
//}

// Password must be at least 6 characters, and contain at least 1 uppercase letter

// function register(password1, password2){
// 	if(password1.length < 6){
// 		alert("Password must be at least 6 chararcters")
// 	}else if(password1 !== password2){
// 		alert("Password must match")
// 	}else{
// 		alert("User registration complete")
// 	}
// }
/*
If score is 5 = S/"Superb"
If score is 3/4 = P/"Pass"
If score is 2 or less = F/"Fail"
*/

// function giveGrade(grade){
// 	if(grade === 5){
// 		console.log("You get an S.")
	// }else if(grade < 5 && grade >= 3){
	// 	console.log("You get a P.")
	// }	
// 	}else if(grade === 3 || grade === 4){
// 		console.log("You get a P.")
// 	}else{
// 		console.log("You get an F.")
// 	}
// }

// ==========
// if(){

// }

// ==========
// if(){

// }else{

// }

// ==========
// if(){

// }else if(){

// }else

// ==========
// if(){

// }else if(){

// }else if(){

// },etc. 

// =========== 

//Mini-Activity: Create a function that will accept a number. If the number given by the user is odd, log in the console "Odd number." If it is even, display "Even number."

// function oddOrEven(num){
// 	if(num % 2 === 1){
// 		console.log("Odd number.")
// 	}else{
// 		console.log("Even number.")
// 	}
// }

// // Data validation = the concept of ensuring that data we receive from the users is of the correct format
// function oddOrEven(num){
// 	if(typeof num === 'string'){
// 		console.log("Invalid data type. Please enter a number.")
// 	}else{
		
// 		if(num % 2 === 1){
// 		console.log("Odd number.")
// 		}else{
// 		console.log("Even number.")
// 		}
	
// 	}
// }

/*
	Mini-Activity:

	Create a function that does the following: 

	Determine typhoon intensity based on the windspeed the user enters (which will be a number)

	1. Windspeed of 30 or below, log a message in the console saying "Not a typhoon yet"

	2. Windspeed of 31 to 61, log a message in the console saying "a tropical depression is detected"

	3. Windspeed of 62 to 88, log a message in the console saying "a tropical storm is detected"

	4. Windspeed of 89 and above, log a message in the console saying "a severe tropical storm is detected"

*/

	// function typhoonIntensity(windspeed){
	// 	if(windspeed <= 30){
	// 		console.log("Not a typhoon yet")
	// 	}else if(windspeed <= 61){
	// 		console.log("Tropical depression detected")
	// 	}else if(windspeed >= 62 && windspeed <=88){
	// 		console.log("Tropical storm detected")
	// 	}else if(windspeed >= 89){
	// 		console.log("Severe tropical storm detected")
	// 	}
	// }

// JS return statements

// function sayNumber(){
// 	console.log(1)
// 	// console.log is ONLY a message, you cannot do anything to it but read it
// }

// function returnNumber(){
// 	return 1()
// 	anything that is returned is still valis JavaSsript data, and can be used for any valid JS purpose. 
// }

// function useReturnNumber(argument){
// 	console.log(returnNumber() + 5)
// }

//TEMPORARY OPERATOR

// function isUnderAge(age){
// 	if(age < 18){
// 		return true;
// 	}else{
// 		return false;
// 	}
// }

// function isUnderAge(age){
// 	return (age < 18) ? true : false; 
// }

// function isNotUnderAge(age){
// 	return (age < 18) ? false : true; 
// }

//Syntax: 
//(condition) ? expression1 : expression2; 
//Ternary operator checks if the condition is true. 
//If yes, it will run expression1
//If no, it will run expression2

//GUIDELINES:
//Ternary operators are a short-hand way to write if-else statements, and so are good for short statements. 
//However, they are generally not as human readable, especially when nested.

//SWITCH STATEMENTS

//If-else statement asks if a condition is true or false

function getFruitByColor(color){
	
	switch(color){
		case "red":
			return "apple"
			break //stop any further code execution
		case "yellow": 
			return "banana"
			break 
		case "orange": 
			return "orange"
			break 
		case "green":
			return "pear"
			break
		default: //if no cases match, run default
			return "unknown fruit"
	}
}

// TRY-CATCH-FINALLY STATEMENT

function tryCatchIntensityAlert(){
	try{
		//Attempt to execute code
		alerat("Tropical storm detected")
	}
	catch(err){
		//catch errors with 'try.' In this case the error is an unknown function "alerat" and we can show the error message in the alert.
		alert(err.message)
	}
 
 	finally{
 		//Continue execution of code regardless of success or failure in try 
 		alert('Code below bad code still works')
 		}	
}

	
